function guardarDatosUsuario() {
  var nombre = txtNombre.value;
  var email = txtEmail.value;
  var dni = txtDni.value;
  var usuario = {"nombre":nombre, "email":email, "dni":dni};
  //var usaurio = {nombre, email, dni} forma simplificada de arriba si los nombres son iguales
  localStorage.setItem("usuarioLocal", JSON.stringify(usuario));
  sessionStorage.setItem("usuarioSession", JSON.stringify(usuario));
}

function recuperarDatosUsuario() {
  var usuarioLocal = localStorage.getItem("usuarioLocal");
  var usuarioSession =  sessionStorage.getItem("usuarioSession");

  alert("usuarioLocal: " + usuarioLocal + " usuarioSession: " + usuarioSession);

  txtNombre.value = JSON.parse(usuarioLocal).nombre;
  txtEmail.value = JSON.parse(usuarioLocal).email;
  txtDni.value = JSON.parse(usuarioLocal).dni;
}
